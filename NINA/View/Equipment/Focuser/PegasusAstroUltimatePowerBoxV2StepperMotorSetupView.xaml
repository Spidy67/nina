﻿<!--
    Copyright © 2016 - 2020 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.Equipment.PegasusAstroUltimatePowerBoxV2StepperMotorSetupView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:local="clr-namespace:NINA.View.Equipment"
    xmlns:locale="clr-namespace:NINA.Locale"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:utility="clr-namespace:NINA.Utility"
    d:DesignHeight="450"
    d:DesignWidth="800"
    mc:Ignorable="d">
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="2*" />
            <ColumnDefinition Width="2*" />
            <ColumnDefinition Width="2*" />
            <ColumnDefinition Width="2*" />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition />
            <RowDefinition />
            <RowDefinition />
            <RowDefinition />
        </Grid.RowDefinitions>
        <TextBlock
            Grid.Row="0"
            Grid.Column="0"
            Margin="5"
            HorizontalAlignment="Right"
            VerticalAlignment="Center"
            Text="{locale:Loc LblSetCurrentPosition}"
            TextWrapping="NoWrap" />
        <TextBox
            x:Name="CurrentPosition"
            Grid.Row="0"
            Grid.Column="1"
            Grid.ColumnSpan="2"
            Margin="5"
            VerticalAlignment="Center" />
        <Button
            Grid.Row="0"
            Grid.Column="3"
            Margin="5"
            VerticalAlignment="Center"
            Command="{Binding SetCurrentPositionCommand}"
            CommandParameter="{Binding Text, ElementName=CurrentPosition}">
            <Button.Content>
                <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{locale:Loc LblSetNewValue}" />
            </Button.Content>
        </Button>
        <TextBlock
            Grid.Row="1"
            Grid.Column="0"
            Margin="5"
            HorizontalAlignment="Right"
            VerticalAlignment="Center"
            Text="{locale:Loc LblBackLashSteps}"
            TextWrapping="NoWrap" />
        <TextBox
            x:Name="BacklashSteps"
            Grid.Row="1"
            Grid.Column="1"
            Grid.ColumnSpan="2"
            Margin="5"
            VerticalAlignment="Center" />
        <Button
            Grid.Row="1"
            Grid.Column="3"
            Margin="5"
            VerticalAlignment="Center"
            Command="{Binding SetBacklashStepsCommand}"
            CommandParameter="{Binding Text, ElementName=BacklashSteps}">
            <Button.Content>
                <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{locale:Loc LblSetNewValue}" />
            </Button.Content>
        </Button>
        <TextBlock
            Grid.Row="2"
            Grid.Column="0"
            Margin="5"
            HorizontalAlignment="Right"
            VerticalAlignment="Center"
            Text="{locale:Loc LblStepperMotorDirection}"
            TextWrapping="NoWrap" />
        <ComboBox
            x:Name="MotorDirection"
            Grid.Row="2"
            Grid.Column="1"
            Grid.ColumnSpan="2"
            Margin="5"
            VerticalAlignment="Center">
            <ComboBoxItem Content="{locale:Loc LblNormal}" Tag="clockWise" />
            <ComboBoxItem Content="{locale:Loc LblReverse}" Tag="antiClockWise" />
        </ComboBox>
        <Button
            Grid.Row="2"
            Grid.Column="3"
            Margin="5"
            VerticalAlignment="Center"
            Command="{Binding SetMotorDirectionCommand}"
            CommandParameter="{Binding ElementName=MotorDirection, Path=SelectedItem.Tag}">
            <Button.Content>
                <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{locale:Loc LblSetNewValue}" />
            </Button.Content>
        </Button>
        <TextBlock
            Grid.Row="3"
            Grid.Column="0"
            Margin="5"
            HorizontalAlignment="Right"
            VerticalAlignment="Center"
            Text="{locale:Loc LblSetMaximumSpeed}"
            TextWrapping="NoWrap" />
        <ComboBox
            x:Name="SelectedSpeed"
            Grid.Row="3"
            Grid.Column="1"
            MinWidth="140"
            Margin="5"
            HorizontalContentAlignment="Center">
            <ComboBoxItem Tag="1000">Starlight HSM/MSM</ComboBoxItem>
            <ComboBoxItem Tag="400">Pegasus Default</ComboBoxItem>
            <ComboBoxItem Tag="200">Low Vibration</ComboBoxItem>
            <ComboBoxItem Tag="0">Custom</ComboBoxItem>
        </ComboBox>
        <TextBox
            x:Name="MaximumSpeed"
            Grid.Row="3"
            Grid.Column="2"
            MinWidth="100"
            Margin="5"
            VerticalAlignment="Center"
            Text="{Binding ElementName=SelectedSpeed, Path=SelectedItem.Tag}" />
        <Button
            Grid.Row="3"
            Grid.Column="3"
            Margin="5"
            VerticalAlignment="Center"
            Command="{Binding SetMaximumSpeedCommand}"
            CommandParameter="{Binding Text, ElementName=MaximumSpeed}">
            <Button.Content>
                <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{locale:Loc LblSetNewValue}" />
            </Button.Content>
        </Button>
    </Grid>
</UserControl>