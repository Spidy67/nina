﻿using System.IO.Ports;
using System.Windows.Controls;

namespace NINA.View.Equipment {

    /// <summary>
    /// Interaction logic for PegasusAstroFlatMasterSetupView.xaml
    /// </summary>
    public partial class PegasusAstroFlatMasterSetupView : UserControl {

        public PegasusAstroFlatMasterSetupView() {
            InitializeComponent();
        }
    }
}